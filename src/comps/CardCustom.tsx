import {
  Card,
  CardContent,
  CardDescription,
  CardFooter,
  CardHeader,
  CardTitle,
} from "@/components/ui/card";
import { Badge } from "@/components/ui/badge";

const CardCustom = ({ name, type, image_url }: Omit<PokemonDetails, "url">) => {
  return (
    <Card className="w-[17rem] bg-[#5a1db5]">
      <CardHeader>
        <CardDescription className="text-xl font-semibold bg-gradient-to-r from-sky-500 to-indigo-400 text-transparent bg-clip-text self-center">
          Pokemon Card
        </CardDescription>
        <CardTitle className="text-2xl capitalize bg-gradient-to-r from-sky-500 to-indigo-400 text-transparent bg-clip-text self-center">
          {name}
        </CardTitle>
      </CardHeader>
      <CardContent className="flex justify-center items-center">
        <img
          src={image_url}
          alt="pokemon image"
          className="rounded-full p-1 bg-slate-900 ring-4 self-center"
        ></img>
      </CardContent>

      <CardContent>
        <p className="text-md font-bold">Type: </p>
        <div className="flex gap-1 decoration-none">
          {type!.map((single_type, idx) => (
            <Badge key={idx}>{single_type}</Badge>
          ))}
        </div>
      </CardContent>
      <CardFooter>
        <p className="text-wrap text-justify text-zinc-400 font-semibold ">
          {`${name} an amazing species of pokemon. A most peculiar Pokémon that somehow appears in a Poké Ball.`}
        </p>
      </CardFooter>
    </Card>
  );
};

export default CardCustom;
