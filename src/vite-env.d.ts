/// <reference types="vite/client" />

interface PokemonDetails {
  name?: string;
  type?: string[];
  image_url?: string;
  url?: string;
}

interface Pokemon {
  name: string;
  url: string;
  type: string[];
  image_url: string;
}

interface PokemonApiResponse {
  count: number;
  next: string | null;
  previous: string | null;
  results: {
    name: string;
    url: string;
  }[];
}

interface Type {
  name: string;
  url: string;
}

interface PokemonType {
  slot: number;
  type: Type;
}
