### To get started :

1. Clone the repository

```sh
git clone git@gitlab.com:br4ke/poki-cards.git
```

2. Check into directory

```sh
cd Poki-Cards
```

3. Install dependencies using the package manager pnpm

```sh
pnpm install || pnpm i
```

4. Run the dev server using

```sh
pnpm dev
```

5. Visit the [local dev server](http:localhost:5173)

---

### Working of Poki App

> The application will auto fetch 60 pokemon infomation and display.
> Our input in the filter input will filter according to the name or types matches of the specified pokemon.


## Working of application
> Fetching pokemon information is straightforward.
> Fetching the images of pokemon according to pokemon index is done from different API than provided.
> The caching for an hour restriction is obeyed by the react-query.

Live 
[@Netlify](https://pokicards.netlify.app/) 

**CI/CD** Failed for Netlify

New Live [@Vercel](https://poki-cards.vercel.app/)